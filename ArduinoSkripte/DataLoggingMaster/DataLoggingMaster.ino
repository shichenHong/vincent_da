// include the libraries
#include <DHT.h>
#include <High_Temp.h>
#include <SPI.h>
#include <SD.h>

// Define the digital pins of the DHT-22
#define DHTPIN_1 26
#define DHTPIN_2 32
#define DHTPIN_3 38
#define DHTPIN_4 44
#define DHTTYPE DHT22

// Declare the thermoelements objects
HighTemp s1(A1, A0);HighTemp s2(A1, A2);
HighTemp s3(A1, A3);HighTemp s4(A1, A4);
HighTemp s5(A1, A5);HighTemp s6(A1, A6);
HighTemp s7(A1, A7);HighTemp s8(A1, A8);

// Declare the DHT-22 objects
DHT dht_1 (DHTPIN_1, DHTTYPE); DHT dht_2 (DHTPIN_2, DHTTYPE);
DHT dht_3 (DHTPIN_3, DHTTYPE); DHT dht_4 (DHTPIN_4, DHTTYPE);

// constant for SD-card
const int chipSelect = 4;

// other pre-declared variable and constants
const int interval = 1000; // 1000 == 1 sec interval between measurement
const int pause = 5000;   // 1000 == 1 sec pause interval

const int nrEinstellung = 1;
const String anmerkung = "";
const String folderName = "29112017";
const String fileNameT = "thermo"+nrEinstellung+anmerkung+".csv";
const String fileNameTRef = "tref"+nrEinstellung+anmerkung+".csv";
const String fileNameDHT = "DHT"+nrEinstellung+anmerkung+".csv";

const char commandBegin = 'b';
const char commandPause = 'p';
const char commandStop = 's';
byte commandByte;
char command;

File root;

// ********************************* Setup **********************************
void setup(){
  Serial.begin(9600);
  while (!Serial) {
    ;
  }
  
  // initialise the thermoelements
  Serial.print("Initialising thermoelements!");
  s1.begin(); s2.begin(); s3.begin(); s4.begin(); 
  s5.begin(); s6.begin(); s7.begin(); s8.begin();
  Serial.println("....... finished");
  Serial.println("***********************************************************");
  
  // initialise the DHT-22
  Serial.print("Initialising DHT-22!");
  dht_1.begin(); dht_2.begin();
  dht_3.begin(); dht_4.begin();
  Serial.println("....... finished");
  Serial.println("***********************************************************");
  
  // initialise the SD-card
  Serial.print("Initializing SD card!");
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    return;
  }
  Serial.println("....... finished");
  Serial.println("***********************************************************");

  // check if the folder and the files to be created alread exist!
  root = SD.open("/");
  if(SD.exists(folderName)){
    Serial.println("Folder " + folderName + " already exists!");
//    Serial.println("If you continue, the existing folder and the files in it will be overwritten!");
//    Serial.println("press y and enter to confirm!");
//    controlByChar('y');
  }else{
    if(SD.mkdir(folderName)){
      Serial.println("Folder was created!" + folderName);
    }else{
      Serial.println("Failed to create folder " + folderName);
    }
  }
  Serial.println("***********************************************************");

  if(SD.exists(folderName + "/" + fileNameT)){
    Serial.println("File already exists!  " + fileNameT);
    Serial.println("If you continue, the existing file will be overwritten!");
    Serial.println("press y and enter to confirm!");
    controlByChar('y');
    SD.remove(folderName + "/" + fileNameT);
    Serial.println(fileNameT + " deleted!");
  }
  Serial.println("***********************************************************");
  
  if(SD.exists(folderName + "/" + fileNameDHT)){
    Serial.println("File already exists!  " + fileNameDHT);
    Serial.println("If you continue, the existing file will be overwritten!");
    Serial.println("press y and enter to confirm!");
    controlByChar('y');
    SD.remove(folderName + "/" + fileNameDHT);
    Serial.println(fileNameDHT + " deleted!");
  }
  Serial.println("***********************************************************");
  
  if(SD.exists(folderName + "/" + fileNameTRef)){
    Serial.println("File already exists!  " + fileNameTRef);
    Serial.println("If you continue, the existing file will be overwritten!");
    Serial.println("press y and enter to confirm!");
    controlByChar('y');
    SD.remove(folderName + "/" + fileNameTRef);
    Serial.println(fileNameTRef + " deleted!");
  }
  Serial.println("***********************************************************");
  
  Serial.println("Press b and enter to start");
}

// ****************************** End of Setup *************************************

void loop(){
  if(Serial.available() > 0){
    commandByte = Serial.read();
    command = char(commandByte);
    Serial.println("Measurement logging started!");
  }


  //  =========================================== Major functionality!============================================================================
  while(command == commandBegin){
   
    Serial.println("Logging running");
//  get Temprature from thermoelement
    float T1 = s1.getThmc();    float T2 = s2.getThmc();
    float T3 = s3.getThmc();    float T4 = s4.getThmc();
    float T5 = s5.getThmc();    float T6 = s6.getThmc();
    float T7 = s7.getThmc();    float T8 = s8.getThmc();
//  get reference Temprature from thermoelement    
    float T_ref = s1.getRoomTmp2(); 
    
//  get Temprature from DHT-22
    float t1 = dht_1.readTemperature();
    float t2 = dht_2.readTemperature();
    float t3 = dht_3.readTemperature();
    float t4 = dht_4.readTemperature();

//  get phi(relative humidity) from DHT-22
    float h1 = dht_1.readHumidity() / 100;
    float h2 = dht_2.readHumidity() / 100;
    float h3 = dht_3.readHumidity() / 100;
    float h4 = dht_4.readHumidity() / 100;

    String oneLineThermo; String oneLineDHT; String oneLineThermo_ref;
    oneLineThermo = pointToComma(T1) + ';'+ pointToComma(T2) + ';'+ pointToComma(T3) + ';'+ pointToComma(T4) + ';'+ pointToComma(T5) + ';'+ pointToComma(T6) + ';'+ pointToComma(T7) + ';' + pointToComma(T8);
    oneLineThermo_ref = pointToComma(T_ref);
    oneLineDHT = pointToComma(t1) + ';'+ pointToComma(t2) + ';'+ pointToComma(t3) + ';'+ pointToComma(t4) + ";0;0;0;0;"+ pointToComma(h1) + ';'+ pointToComma(h2) + ';'+ pointToComma(h3) + ';' + pointToComma(h4);

    Serial.println("----------------------------------------------");
    Serial.print("Thermoelement --- T1 to T8: "); Serial.println(oneLineThermo);
    Serial.print("DHT22 --- t1 to t8 and h1 to h8: "); Serial.println(oneLineDHT);
    Serial.print("The ref temperatur: "); Serial.println(oneLineThermo_ref);
    writeLine2SD(folderName + "/" + fileNameT, oneLineThermo);
    writeLine2SD(folderName + "/" + fileNameDHT, oneLineDHT);
    writeLine2SD(folderName + "/" + fileNameTRef, oneLineThermo_ref);

    Serial.println("-------------------End of 1 measurement---------------------------");
    delay(interval);
    
//    provide a chance for input and thus to stop
    if(Serial.available() > 0){
      commandByte = Serial.read();
      command = char(commandByte);
    }
  }
  //  =========================================== End of major functionality ============================================================================  
  
//  insert p for pause, insert b again to start again.
  while(command == commandPause){

    Serial.print("-------------------- Pause ---");
    delay(pause);
    Serial.println("----- 5 secs ------------");
    if(Serial.available() > 0){
        commandByte = Serial.read();
        command = char(commandByte);
      }   
  }
  
//  insert s for stop
  if(command == commandStop){
    Serial.println("Logging stopped!");
    while (true); // The script ends here.
  }
}


//Other functions

String pointToComma(float num){
  String Num = String(num, 2); // 2 means 2 numbers after decimal point
  Num.replace('.', ',');
  return(Num);
}

void writeLine2SD(String fileName, String line){
  File dataFile = SD.open(fileName, FILE_WRITE);
  if (dataFile) {
    dataFile.println(line);
    dataFile.close();
    Serial.print("A line written to file >>> "); Serial.println(fileName);
  }
  else {
    Serial.println("error opening file " + fileName);
  }
}

void controlByChar(char command){
  char key;
  while(key != command){
    if(Serial.available() > 0){
      key = char(Serial.read());
      Serial.println(key);
    }
  }
  Serial.println("Confirmed");
  Serial.println(" ");
}

