#include <SPI.h>
#include <SD.h>

File myFile;
const String fileName = "05122017/thermo1.CSV";
//const String fileName = "28112017/dht1.CSV";
//const String fileName = "28112017/tref1.CSV";

void setup() {
  Serial.begin(9600);
  while (!Serial) {
    ;
  }
  
  Serial.print("Initializing SD card...");
  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  myFile = SD.open(fileName);
  if (myFile) {
    Serial.print("The content of file ");
    Serial.println(fileName);
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    myFile.close();
  } else {
    Serial.print("error opening ");
    Serial.println(fileName);
  }
}

void loop() {

}
