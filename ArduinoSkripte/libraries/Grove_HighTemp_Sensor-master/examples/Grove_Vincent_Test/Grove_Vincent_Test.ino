// demo of Grove - High Temperature Sensor
// Sensor_1, intern A1, extern A0. A1 ist gemeinsam für alle
// Sensor_2, extern A2; ... ; Sensor_(n), extern A(n)

// HighTemp(int _pinTmp, int _pinThmc)
#include "High_Temp.h"

HighTemp s1(A1, A0);HighTemp s2(A1, A2);
//HighTemp s3(A1, A3);HighTemp s4(A1, A4);
//HighTemp s5(A1, A5);HighTemp s6(A1, A6);
//HighTemp s7(A1, A7);HighTemp s8(A1, A8);
//HighTemp s9(A1, A9);HighTemp s10(A1, A10);
//HighTemp s11(A1, A11);HighTemp s12(A1, A12);


HighTemp ht2_ref(A3, A2);
  
void setup()
{
    Serial.begin(115200);
    Serial.println("grove - hight temperature sensor test demo");
    s1.begin(); s2.begin(); 
    //s3.begin(); s4.begin(); s5.begin(); s6.begin(); s7.begin(); 
    //s8.begin(); s9.begin(); s10.begin(); s11.begin(); s12.begin();
}

void loop()
{
    delay(3000); // 3 Sekunde Intervall
    
    Serial.print("T_room,1 = ");Serial.print(s1.getRoomTmp2());
    Serial.print("; T_thermo,1 = ");Serial.println(s1.getThmc());
    Serial.print("T_room,2 = ");Serial.print(s2.getRoomTmp2());   
    Serial.print("; T_thermo,2 = ");Serial.println(s2.getThmc());

//    Serial.print("T_room,3 = ");Serial.print(s3.getRoomTmp2());
//    Serial.print("T_thermo,3 = ");Serial.println(s3.getThmc());
//    Serial.print("T_room,4 = ");Serial.println(s4.getRoomTmp2());   
//    Serial.print("T_thermo,4 = ");Serial.println(s4.getThmc());
//
//    Serial.print("T_room,5 = ");Serial.print(s5.getRoomTmp2());
//    Serial.print("T_thermo,5 = ");Serial.println(s5.getThmc());
//    Serial.print("T_room,6 = ");Serial.println(s6.getRoomTmp2());   
//    Serial.print("T_thermo,6 = ");Serial.println(s6.getThmc());
//
//    Serial.print("T_room,7 = ");Serial.print(s7.getRoomTmp2());
//    Serial.print("T_thermo,7 = ");Serial.println(s7.getThmc());
//    Serial.print("T_room,8 = ");Serial.println(s8.getRoomTmp2());   
//    Serial.print("T_thermo,8 = ");Serial.println(s8.getThmc());
//
//    Serial.print("T_room,9 = ");Serial.print(s9.getRoomTmp2());
//    Serial.print("T_thermo,9 = ");Serial.println(s9.getThmc());
//    Serial.print("T_room,10 = ");Serial.println(s10.getRoomTmp2());   
//    Serial.print("T_thermo,10 = ");Serial.println(s10.getThmc());
//
//    Serial.print("T_room,11 = ");Serial.print(s11.getRoomTmp2());
//    Serial.print("T_thermo,11 = ");Serial.println(s11.getThmc());
//    Serial.print("T_room,12 = ");Serial.println(s12.getRoomTmp2());   
//    Serial.print("T_thermo,12 = ");Serial.println(s12.getThmc());
}
