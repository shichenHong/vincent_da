// include the libraries
#include <DHT.h>
#include <High_Temp.h>
#include <SPI.h>
#include <SD.h>
#include <Arduino.h>
#include <Wire.h>         // this #include still required because the RTClib depends on it
#include "RTClib.h"

#if defined(ARDUINO_ARCH_SAMD)
// for Zero, output on USB Serial console, remove line below if using programming port to program the Zero!
   #define Serial SerialUSB
#endif


// Define the digital pins of the DHT-22
#define DHTPIN_1 26
#define DHTPIN_2 32
#define DHTPIN_3 38
#define DHTPIN_4 44
#define DHTTYPE DHT22

//Declare the rtc for timestamp
RTC_Millis rtc;


// Declare the thermoelements objects
HighTemp s1(A1, A0);HighTemp s2(A1, A2);
HighTemp s3(A1, A3);HighTemp s4(A1, A4);
HighTemp s5(A1, A5);HighTemp s6(A1, A6);
HighTemp s7(A1, A7);HighTemp s8(A1, A8);

// Declare the DHT-22 objects
DHT dht_1 (DHTPIN_1, DHTTYPE); DHT dht_2 (DHTPIN_2, DHTTYPE);
DHT dht_3 (DHTPIN_3, DHTTYPE); DHT dht_4 (DHTPIN_4, DHTTYPE);

// constant for SD-card
const int chipSelect = 4;

// other pre-declared variable and constants
const int interval = 5000; // 1000 == 1 sec interval between measurement
const int pause = 5000;   // 1000 == 1 sec pause interval

const String nrEinstellung = "3";
const String anmerkung = "";
const String folderName = "08122017";
const String fileNameT = "thermo"+nrEinstellung+anmerkung+".csv";
const String fileNameTRef = "tref"+nrEinstellung+anmerkung+".csv";
const String fileNameDHT = "DHT"+nrEinstellung+anmerkung+".csv";

const char commandBegin = 'b';
const char commandPause = 'p';
const char commandStop = 's';
byte commandByte;
char command;
int i = 0;
File root;


// ********************************* Setup **********************************
void setup(){
  Serial.begin(9600);
  while (!Serial) ;

  //  initialiste the rtc
rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
  
  
  // initialise the thermoelements
  Serial.print("Initialising thermoelements!");
  s1.begin(); s2.begin(); s3.begin(); s4.begin(); 
  s5.begin(); s6.begin(); s7.begin(); s8.begin();
  Serial.println("....... finished");
  Serial.println("***********************************************************");
  
  // initialise the DHT-22
  Serial.print("Initialising DHT-22!");
  dht_1.begin(); dht_2.begin();
  dht_3.begin(); dht_4.begin();
  Serial.println("....... finished");
  Serial.println("***********************************************************");
  
  // initialise the SD-card
  Serial.print("Initializing SD card!");
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    return;
  }
  Serial.println("....... finished");
  Serial.println("***********************************************************");

  // check if the folder and the files to be created alread exist!
  root = SD.open("/");
  if(SD.exists(folderName)){
    Serial.println("Folder " + folderName + " already exists!");
  }else{
    if(SD.mkdir(folderName)){
      Serial.println("Folder was created!" + folderName);
    }else{
      Serial.println("Failed to create folder " + folderName);
    }
  }
  Serial.println("***********************************************************");

  if(SD.exists(folderName + "/" + fileNameT)){
    Serial.println("File already exists!  " + fileNameT);
    Serial.println("If you continue, the existing file will be overwritten!");
    Serial.println("press y and enter to confirm!");
    controlByChar('y');
    SD.remove(folderName + "/" + fileNameT);
    Serial.println(fileNameT + " deleted!");
  }
  Serial.println("***********************************************************");
  
  if(SD.exists(folderName + "/" + fileNameDHT)){
    Serial.println("File already exists!  " + fileNameDHT);
    Serial.println("If you continue, the existing file will be overwritten!");
    Serial.println("press y and enter to confirm!");
    controlByChar('y');
    SD.remove(folderName + "/" + fileNameDHT);
    Serial.println(fileNameDHT + " deleted!");
  }
  Serial.println("***********************************************************");
  
  if(SD.exists(folderName + "/" + fileNameTRef)){
    Serial.println("File already exists!  " + fileNameTRef);
    Serial.println("If you continue, the existing file will be overwritten!");
    Serial.println("press y and enter to confirm!");
    controlByChar('y');
    SD.remove(folderName + "/" + fileNameTRef);
    Serial.println(fileNameTRef + " deleted!");
  }
  Serial.println("***********************************************************");
  
  Serial.println("Press b and enter to start");
}

// ****************************** End of Setup *************************************

void loop(){

  
  if(Serial.available() > 0){
    commandByte = Serial.read();
    command = char(commandByte);
    Serial.println("You just entered: " + command);
  }




  //  =========================================== Major functionality!============================================================================

  while(command == commandBegin){
    Serial.println("Logging running");
//  get timestamp
//    if(i == 0){
       DateTime ts = rtc.now();
//    }
    
    String timestamp = String(ts.hour(), DEC) + ':' + String(ts.minute(), DEC) + ':' + String(ts.second(), DEC);
    
//  get Temprature from thermoelement
    float T1 = s1.getThmc();    float T2 = s2.getThmc();
    float T3 = s3.getThmc();    float T4 = s4.getThmc();
    float T5 = s5.getThmc();    float T6 = s6.getThmc();
    float T7 = s7.getThmc();    float T8 = s8.getThmc();
//  get reference Temprature from thermoelement    
    float T_ref = s1.getRoomTmp2(); 
    
//  get Temprature from DHT-22
    float t1 = dht_1.readTemperature();
    float t2 = dht_2.readTemperature();
    float t3 = dht_3.readTemperature();
    float t4 = dht_4.readTemperature();

//  get phi(relative humidity) from DHT-22
    float h1 = dht_1.readHumidity() / 100;
    float h2 = dht_2.readHumidity() / 100;
    float h3 = dht_3.readHumidity() / 100;
    float h4 = dht_4.readHumidity() / 100;

    String oneLineThermo; String oneLineDHT; String oneLineThermo_ref;
    oneLineThermo = timestamp + ';' + pointToComma(T1) + ';'+ pointToComma(T2) + ';'+ pointToComma(T3) + ';'+ pointToComma(T4) + ';'+ pointToComma(T5) + ';'+ pointToComma(T6) + ';'+ pointToComma(T7) + ';' + pointToComma(T8);
    oneLineThermo_ref = timestamp + ';' + pointToComma(T_ref);
    oneLineDHT = timestamp + ';' + pointToComma(t1) + ';'+ pointToComma(t2) + ';'+ pointToComma(t3) + ';'+ pointToComma(t4) + ';'+ pointToComma(h1) + ';'+ pointToComma(h2) + ';'+ pointToComma(h3) + ';' + pointToComma(h4);

    Serial.println("----------------------------------------------");
    Serial.print("Thermoelement --- T1 to T8: "); Serial.println(oneLineThermo);
    Serial.print("DHT22 --- t1 to t8 and h1 to h8: "); Serial.println(oneLineDHT);
    Serial.print("The ref temperatur: "); Serial.println(oneLineThermo_ref);
    
//    Create header
    if(i == 0){
      String headerThermo; String headerThermo_ref = "Time; T_ref"; String headerDHT;
//      for(int k = 1; k <=8; k ++){
//        headerThermo = headerThermo + "; T" + char(k);
//        headerDHT = headerDHT + "; T" + char(k);
//      }
//
//      for(int k = 1; k <=8; k ++){
//        headerDHT = headerDHT + "; h" + char(k);
//      }
      headerThermo = "Time; T1; T2; T3; T4; T5; T6; T7; T8";
      headerDHT = "Time; T1; T2; T3; T4; h1; h2; h3; h4";
      
      writeLine2SD(folderName + "/" + fileNameT, headerThermo);
      writeLine2SD(folderName + "/" + fileNameDHT, headerDHT);
      writeLine2SD(folderName + "/" + fileNameTRef, headerThermo_ref);
    }
    
    writeLine2SD(folderName + "/" + fileNameT, oneLineThermo);
    writeLine2SD(folderName + "/" + fileNameDHT, oneLineDHT);
    writeLine2SD(folderName + "/" + fileNameTRef, oneLineThermo_ref);

    Serial.println("-------------------End of 1 measurement---------------------------");
    i = i + 1;
    delay(interval);
    
//    provide a chance for input and thus to stop
    if(Serial.available() > 0){
      commandByte = Serial.read();
      command = char(commandByte);
    }
  }
  //  =========================================== End of major functionality ============================================================================  
  
//  insert p for pause, insert b again to start again.
  while(command == commandPause){

    Serial.print("-------------------- Pause ---");
    delay(pause);
    Serial.println("----- 5 secs ------------");
    if(Serial.available() > 0){
        commandByte = Serial.read();
        command = char(commandByte);
      }   
  }
  
//  insert s for stop
  if(command == commandStop){
    Serial.println("Logging stopped!");
    while (true); // The script ends here.
  }
}


//Other functions

String pointToComma(float num){
  String Num = String(num, 2); // 2 means 2 numbers after decimal point
  Num.replace('.', ',');
  return(Num);
}

void writeLine2SD(String fileName, String line){
  File dataFile = SD.open(fileName, FILE_WRITE);
  if (dataFile) {
    dataFile.println(line);
    dataFile.close();
    Serial.print("A line written to file >>> "); Serial.println(fileName);
  }
  else {
    Serial.println("error opening file " + fileName);
  }
}

void controlByChar(char command){
  char key;
  while(key != command){
    if(Serial.available() > 0){
      key = char(Serial.read());
      Serial.println(key);
    }
  }
  Serial.println("Confirmed");
  Serial.println(" ");
}

